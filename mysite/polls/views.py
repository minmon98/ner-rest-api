from underthesea import ner
from django.http import JsonResponse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json

@csrf_exempt
def home(request):
    name = ""
    sentences = json.loads(request.body)['sentences']
    for sentence in sentences:
        result = ner(sentence)
        for word in result:
            if "PER" in word[3]:
                name += word[0] + " "
    return JsonResponse({
        'name': name
    })